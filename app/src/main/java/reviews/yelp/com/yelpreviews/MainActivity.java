package reviews.yelp.com.yelpreviews;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.yelp.clientlib.entities.Business;
import com.yelp.clientlib.entities.SearchResponse;
import com.yelp.clientlib.entities.options.CoordinateOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import reviews.yelp.com.yelpreviews.adapter.SearchResultAdapter;
import reviews.yelp.com.yelpreviews.callbackinterface.OnClickListener;
import reviews.yelp.com.yelpreviews.callbackinterface.SearchResultsCallback;
import reviews.yelp.com.yelpreviews.common.Constants;

public class MainActivity extends AppCompatActivity
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        SearchResultsCallback, OnClickListener
{

    public final int PERMISSION_REQUEST = 123;

    private GoogleApiClient mGoogleApiClient;
    protected static final String TAG = "MainActivity";

    protected Location mLastLocation = null;

    private RecyclerView mRecyclerViewSearch;
    private TextView mEmptyView;

    //Set default sortby to 0 (best matched)
    private int sortby = 0;
    private String searchTerm = "food";

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerViewSearch = (RecyclerView)findViewById(R.id.recyclerViewSearch);
        mEmptyView = (TextView) findViewById(R.id.tvEmptyView);
        mEmptyView.setVisibility(View.GONE);

        mProgressDialog = new ProgressDialog(MainActivity.this);

        mRecyclerViewSearch.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        if (hasRequiredPermissions())
        {
            buildGoogleApiClient();
        }

        //Do an initial search with default values, so user is not looking at a blank page
        doSearch();

        handleIntent(getIntent());
    }

    /**
     * Will call the YelpAPI Search function with searchTerm and sortby.
     * Shows the loading spinner for user feedback.
     */
    private void doSearch()
    {
        Map<String, String> params = new HashMap<>();
        params.put("term", searchTerm);
        params.put("sort", String.valueOf(sortby));
        params.put("limit", String.valueOf(Constants.SEARCH_LIMIT));

        CoordinateOptions coordinate = null;

        if (mLastLocation != null)
        {
            coordinate = CoordinateOptions.builder()
                    .latitude(mLastLocation.getLatitude())
                    .longitude(mLastLocation.getLongitude()).build();
        }

        mProgressDialog.setMessage("loading...");
        mProgressDialog.show();
        mEmptyView.setVisibility(View.GONE);

        Controller.getInstance().search(coordinate, params, this);
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    /**
     * Called when the user searches using the searchbar.
     * @param intent
     */
    private void handleIntent(Intent intent)
    {
        if (Intent.ACTION_SEARCH.equals(intent.getAction()))
        {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchTerm = query;
            doSearch();
        }
    }

    /**
     * Checks to see if the required permissions are present.
     *
     * @return
     *      - True: If permissions are present
     *      - False: otherwise
     */
    private boolean hasRequiredPermissions()
    {
        List<String> permissionsRequired = new ArrayList<String>();

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                //The user has denied this permission before, we could give them an explanation.
            } else {
                //Ask for permission
                permissionsRequired.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            }
        }

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION)) {

            } else {

                //Ask for permission
                permissionsRequired.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
            }
        }

        if (permissionsRequired.size() > 0)
        {
            ActivityCompat.requestPermissions(MainActivity.this,
                    permissionsRequired.toArray(new String[permissionsRequired.size()]),
                    PERMISSION_REQUEST);
        }
        else
        {
            return true;
        }

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {

        if (requestCode == PERMISSION_REQUEST)
        {
            boolean allPermissionGranted = true;

            for (int i : grantResults)
            {
                if (i != PackageManager.PERMISSION_GRANTED)
                {
                    allPermissionGranted = false;
                    break;
                }
            }

            if (allPermissionGranted)
            {
                buildGoogleApiClient();
            }
        }
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint)
    {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(MainActivity.this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION )
                        != PackageManager.PERMISSION_GRANTED
                &&
                ContextCompat.checkSelfPermission(MainActivity.this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null)
        {
            Log.i(TAG, "Lat: " + mLastLocation.getLatitude());
            Log.i(TAG, "Lon: " + mLastLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result)
    {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause)
    {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void searchResultsCallback(SearchResponse searchResponse, boolean isErrorOccured)
    {

        mProgressDialog.dismiss();

        if (!isErrorOccured && searchResponse != null && searchResponse.businesses().size() > 0)
        {
            mEmptyView.setVisibility(View.GONE);
            mRecyclerViewSearch.setVisibility(View.VISIBLE);

            SearchResultAdapter adapter = new SearchResultAdapter(searchResponse, MainActivity.this, this);
            mRecyclerViewSearch.setAdapter(adapter);
        }
        else
        {
            mEmptyView.setVisibility(View.VISIBLE);
            mRecyclerViewSearch.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.sort)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Sort by");
            builder.setItems(
                    Constants.SORT_BY_OPTIONS,
                    new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            sortby = which;
                            //This will search every time the user selects a different sort-by
                            doSearch();
                        }
                    });
            builder.show();
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        final MenuItem searchMenuItem = menu.findItem(R.id.search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                //Close the menuitem when user presses the search button
                MenuItemCompat.collapseActionView(searchMenuItem);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                return false;
            }
        });

        return true;
    }

    @Override
    public void onClick(int index)
    {
        Intent intent = new Intent(MainActivity.this, BusinessDetailActivity.class);
        intent.putExtra(Constants.TAG_BUSINESS_ID, ((Business)((SearchResultAdapter)mRecyclerViewSearch.getAdapter()).getItem(index)).id());
        startActivity(intent);
    }
}
