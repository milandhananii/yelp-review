package reviews.yelp.com.yelpreviews.callbackinterface;

import com.yelp.clientlib.entities.SearchResponse;

/**
 * Created by Milan on 2016-05-25.
 */
public interface SearchResultsCallback
{
    void searchResultsCallback(SearchResponse searchResponse, boolean isErrorOccured);
}
