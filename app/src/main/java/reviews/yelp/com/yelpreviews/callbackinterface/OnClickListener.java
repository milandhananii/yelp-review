package reviews.yelp.com.yelpreviews.callbackinterface;

/**
 * Created by Milan on 2016-05-26.
 */
public interface OnClickListener
{
    void onClick(int index);
}
