package reviews.yelp.com.yelpreviews;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.yelp.clientlib.entities.Business;
import com.yelp.clientlib.entities.Category;

import java.util.ArrayList;
import java.util.List;

import reviews.yelp.com.yelpreviews.callbackinterface.BusinessResultCallback;
import reviews.yelp.com.yelpreviews.common.Constants;

public class BusinessDetailActivity extends AppCompatActivity implements BusinessResultCallback
{

    public final int PERMISSION_REQUEST = 456;

    private ProgressDialog mProgressDialog;
    private String mBusinessPhone = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_detail);

        mProgressDialog = new ProgressDialog(BusinessDetailActivity.this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        String business_id = getIntent().getStringExtra(Constants.TAG_BUSINESS_ID);

        if (business_id != null && !business_id.isEmpty())
        {
            //Get the business information
            mProgressDialog.setMessage("loading...");
            mProgressDialog.show();
            Controller.getInstance().findBusinessWithBusinessId(business_id, this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            navigateUpTo(new Intent(this, MainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void businessResultsCallback(final Business business, boolean isErrorOccured)
    {
        mProgressDialog.dismiss();

        if (!isErrorOccured && business != null)
        {

            //Request a larger image file, Yelp will send the ms.jpg (small) by default
            if (business.imageUrl() != null && !business.imageUrl().isEmpty())
            {
                String imageUrl = business.imageUrl().replace("ms.jpg", "l.jpg");

                Picasso.with(BusinessDetailActivity.this)
                        .load(imageUrl)
                        .placeholder(R.mipmap.business_placeholder_image)
                        .error(R.mipmap.business_placeholder_image)
                        .fit()
                        .tag(BusinessDetailActivity.this)
                        .into((ImageView)findViewById(R.id.ivBusinessImage));
            }

            ((TextView)findViewById(R.id.tvBusinessName)).setText(business.name());
            ((RatingBar)findViewById(R.id.businessRating)).setRating(business.rating().floatValue());

            //Address
            if (business.location() != null && business.location().address() != null
                    && business.location().address().size() > 0)
            {
                //Assume the first address
                ((TextView)findViewById(R.id.tvAddress)).setText(business.location().address().get(0));
                findViewById(R.id.ivMap).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                        double lat = business.location().coordinate().latitude();
                        double lon = business.location().coordinate().longitude();

                        Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(String.format("geo:%s,%s?q=%s,%s(" + business.name() + ")", lat, lon, lat, lon)));
                        startActivity(mapIntent);
                    }
                });
            }
            else
            {
                //Something else should probably happen here.
                ((TextView)findViewById(R.id.tvAddress)).setText("No address found.");
            }

            //Phone number
            if (business.phone() != null && !business.phone().isEmpty())
            {
                findViewById(R.id.ivCall).setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (hasRequiredPermissions())
                        {
                            Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + business.phone()));
                            startActivity(callIntent);
                        }
                        else
                        {
                            //Ask for permission and finish the call action.
                            requestPermission(business.phone());
                        }
                    }
                });
            }
            else
            {
                (findViewById(R.id.ivCall)).setAlpha(0.2f);
                (findViewById(R.id.ivCall)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        Toast.makeText(BusinessDetailActivity.this, R.string.no_phone_number, Toast.LENGTH_LONG).show();
                    }
                });
            }

            if (business.categories() != null && business.categories().size() > 0)
            {
                StringBuffer categories = new StringBuffer();

                for (Category category : business.categories())
                {
                    categories.append(", " + category.name());
                }

                if (!categories.toString().isEmpty())
                {
                    //Delete the first comma and space from categories.
                    if (categories.length() >= 2)
                    {
                        categories = categories.deleteCharAt(0);
                        categories = categories.deleteCharAt(0);
                    }

                    ((TextView)findViewById(R.id.tvCategory)).setText(categories.toString());
                }
            }
            else
            {
                findViewById(R.id.tvCategory).setVisibility(View.GONE);
            }
        }
    }

    /**
     * @return
     *      - True: If CALL_PHONE permission is granted
     *      - False: otherwise
     */
    private boolean hasRequiredPermissions()
    {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(BusinessDetailActivity.this,
                        Manifest.permission.CALL_PHONE )
                        == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        return false;
    }

    /**
     * Requests CALL_PHONE permission
     * @param businessPhone
     */
    private void requestPermission(String businessPhone)
    {
        //Keep a reference because we'll have to call it from onRequestPermissionsResult
        mBusinessPhone = businessPhone;

        List<String> permissionsRequired = new ArrayList<String>();

        if (ContextCompat.checkSelfPermission(BusinessDetailActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(BusinessDetailActivity.this,
                    android.Manifest.permission.CALL_PHONE)) {
                //The user has denied this permission before, we could give them an explanation.
            } else {
                //Ask for permission
                permissionsRequired.add(android.Manifest.permission.CALL_PHONE);
            }
        }

        if (permissionsRequired.size() > 0)
        {
            ActivityCompat.requestPermissions(BusinessDetailActivity.this,
                    permissionsRequired.toArray(new String[permissionsRequired.size()]),
                    PERMISSION_REQUEST);
        }
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {

        if (requestCode == PERMISSION_REQUEST)
        {
            boolean allPermissionGranted = true;

            for (int i : grantResults)
            {
                if (i != PackageManager.PERMISSION_GRANTED)
                {
                    allPermissionGranted = false;
                    break;
                }
            }

            if (allPermissionGranted && mBusinessPhone != null)
            {
                //re-try opening the Phone activity
                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mBusinessPhone));
                startActivity(callIntent);
            }
        }
    }
}
