package reviews.yelp.com.yelpreviews.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yelp.clientlib.entities.Business;
import com.yelp.clientlib.entities.Category;
import com.yelp.clientlib.entities.SearchResponse;

import reviews.yelp.com.yelpreviews.R;
import reviews.yelp.com.yelpreviews.callbackinterface.OnClickListener;

/**
 * Created by Milan on 2016-05-25.
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder>
{

    private SearchResponse data;
    Context mContext;
    private static OnClickListener mOnClickListener;

    public SearchResultAdapter(SearchResponse data, Context context, OnClickListener onClickListener)
    {
        this.data = data;
        this.mContext = context;
        this.mOnClickListener = onClickListener;
    }

    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_search_view, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Business business = data.businesses().get(position);

        holder.mTextViewBusinessName.setText(business.name());
        holder.mTextViewAddress.setText(business.location().address().get(0));
        holder.mRatingBar.setRating(business.rating().floatValue());
        holder.mNumberOfReviews.setText(business.reviewCount() + " reviews");

        if (business.categories() != null && business.categories().size() > 0)
        {
            StringBuffer categories = new StringBuffer();

            for (Category category : business.categories())
            {
                categories.append(", " + category.name());
            }

            if (!categories.toString().isEmpty())
            {
                //Delete the first comma and space from categories.
                if (categories.length() >= 2)
                {
                    categories = categories.deleteCharAt(0);
                    categories = categories.deleteCharAt(0);
                }

                holder.mTextViewCategory.setText(categories.toString());
            }
        }
        else
        {
            holder.mTextViewCategory.setVisibility(View.GONE);
        }

        //Load the pictures asynchronously
        Picasso.with(mContext)
                .load(business.imageUrl())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .fit()
                .tag(mContext)
                .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return data.businesses().size();
    }

    public Business getItem(int index)
    {
        if (data == null || data.businesses().size() < 1)
            return null;

        return data.businesses().get(index);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        // each data item is just a string in this case
        public TextView mTextViewAddress, mTextViewBusinessName, mTextViewCategory, mNumberOfReviews;
        public ImageView mImageView;
        public RatingBar mRatingBar;
        public boolean imageSet;
        public ViewHolder(View v)
        {
            super(v);
            v.setOnClickListener(this);
            mTextViewAddress = (TextView) v.findViewById(R.id.tvAddress);
            mTextViewBusinessName = (TextView) v.findViewById(R.id.tvBusinessName);
            mTextViewCategory = (TextView) v.findViewById(R.id.tvCategory);
            mNumberOfReviews = (TextView) v.findViewById(R.id.tvNumberOfReviews);
            mRatingBar = (RatingBar) v.findViewById(R.id.ratingBar);
            mImageView = (ImageView) v.findViewById(R.id.ivBusinessImage);
            imageSet = false;
        }

        @Override
        public void onClick(View v)
        {
            SearchResultAdapter.mOnClickListener.onClick(getAdapterPosition());
        }
    }
}
