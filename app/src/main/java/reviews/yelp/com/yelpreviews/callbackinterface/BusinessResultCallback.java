package reviews.yelp.com.yelpreviews.callbackinterface;

import com.yelp.clientlib.entities.Business;

/**
 * Created by Milan on 2016-05-25.
 */
public interface BusinessResultCallback
{
    void businessResultsCallback(Business business, boolean isErrorOccured);
}
