package reviews.yelp.com.yelpreviews;

import com.yelp.clientlib.connection.YelpAPI;
import com.yelp.clientlib.connection.YelpAPIFactory;

import reviews.yelp.com.yelpreviews.common.Constants;

/**
 * Created by Milan on 2016-05-25.
 *
 * This is a singleton class.
 * We only really need one instance of the YelpAPI object.
 */
public class MyYelpAPI
{
    private static YelpAPI instance = null;

    public static YelpAPI getInstance()
    {
        if (instance == null)
        {
            YelpAPIFactory apiFactory = new YelpAPIFactory(Constants.CONSUMER_KEY,
                    Constants.CONSUMER_SECRETE,
                    Constants.TOKEN,
                    Constants.TOKEN_SECRETE);

            instance = apiFactory.createAPI();
        }
        return instance;
    }

}
