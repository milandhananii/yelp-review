package reviews.yelp.com.yelpreviews;

import com.yelp.clientlib.connection.YelpAPI;
import com.yelp.clientlib.entities.Business;
import com.yelp.clientlib.entities.SearchResponse;
import com.yelp.clientlib.entities.options.CoordinateOptions;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import reviews.yelp.com.yelpreviews.callbackinterface.BusinessResultCallback;
import reviews.yelp.com.yelpreviews.callbackinterface.SearchResultsCallback;

/**
 * Created by Milan on 2016-05-25.
 */
public class Controller
{
    private YelpAPI mYelpApi;
    private static Controller instance = null;

    private Controller()
    {
        mYelpApi = MyYelpAPI.getInstance();
    }

    public static Controller getInstance()
    {
        if (instance == null)
        {
            instance = new Controller();
        }

        return instance;
    }

    /**
     * Searches the YelpAPI to get the list of businesses around the given coordinates.
     * @param coordinateOptions
     * @param searchParams
     * @param callback
     */
    public void search(CoordinateOptions coordinateOptions,
                       Map<String, String> searchParams,
                       final SearchResultsCallback callback)
    {
        if (searchParams != null && searchParams.size() > 0)
        {
            //Search the SearchAPI
            Call<SearchResponse> call;

            if (coordinateOptions == null)
                call = mYelpApi.search("Toronto", searchParams);
            else
                call = mYelpApi.search(coordinateOptions, searchParams);

            Callback<SearchResponse> searchResponseCallback = new Callback<SearchResponse>()
            {
                @Override
                public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response)
                {
                    callback.searchResultsCallback(response.body(), false);
                }
                @Override
                public void onFailure(Call<SearchResponse> call, Throwable t)
                {
                    callback.searchResultsCallback(null, true);
                }
            };

            call.enqueue(searchResponseCallback);
        }
    }

    /**
     * Looks up the specific business given the BusinessID.
     * @param id
     * @param callback
     */
    public void findBusinessWithBusinessId(String id,
                                           final BusinessResultCallback callback)
    {
        if (id != null && !id.isEmpty())
        {
            //Search the business by ID
            Call<Business> call = mYelpApi.getBusiness(id);

            Callback<Business> searchResponseCallback = new Callback<Business>()
            {
                @Override
                public void onResponse(Call<Business> call, Response<Business> response)
                {
                    callback.businessResultsCallback(response.body(), false);
                }
                @Override
                public void onFailure(Call<Business> call, Throwable t)
                {
                    callback.businessResultsCallback(null, true);
                }
            };

            call.enqueue(searchResponseCallback);
        }
    }
}
